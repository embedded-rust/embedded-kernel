#![no_std]
#![no_main]
#![feature(asm)]

// use panic_halt as _;
use panic_semihosting as _;

use stm32f1xx_hal::{
    delay::Delay,
    gpio::*,
    pac,
    prelude::*,
};

use embedded_hal::digital::v2::OutputPin;

use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;

use cortex_m_rt::exception;

#[exception]
fn SVCall() {
    hprintln!("inside the SVC").unwrap();
}

#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let mut flash = dp.FLASH.constrain();
    let mut rcc = dp.RCC.constrain();

    let clocks = rcc.cfgr.freeze(&mut flash.acr);

    let mut gpioc = dp.GPIOC.split(&mut rcc.apb2);

    let mut led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);

    let mut delay = Delay::new(cp.SYST, clocks);

    unsafe {asm!{"svc 12"}};
    // From http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dai0179b/ar01s02s07.html
    // You can change between Privileged Thread mode and User Thread mode when
    // returning from an exception by modifying the EXC_RETURN value in the link
    // register (R14). You can also change from Privileged Thread to User Thread
    // mode by clearing CONTROL[0] using an MSR instruction. However, you cannot
    // directly change to privileged mode from unprivileged mode without going
    // through an exception, for example an SVC

    loop {
        led.set_high().unwrap();
        delay.delay_ms(100 as u32);
        led.set_low().unwrap();
        delay.delay_ms(100 as u32);
    }
}
